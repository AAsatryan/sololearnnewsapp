package com.example.newsapp.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey


@Entity(tableName = "news_table")
data class NewsRoom(
    @PrimaryKey
    var id: String,
    var trailText: String?,
    var thumbnail: String?,
    var sectionName: String?,
    var article: String?
)
