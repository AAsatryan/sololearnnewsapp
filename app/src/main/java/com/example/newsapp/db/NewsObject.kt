package com.example.newsapp.db

import java.io.Serializable


data class NewsObject(
    var id: String?,
    var trailText: String?,
    var thumbnail: String?,
    var sectionName: String?,
    var article: String?
)
