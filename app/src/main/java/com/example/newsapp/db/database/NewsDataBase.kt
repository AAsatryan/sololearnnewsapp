package com.example.newsapp.db.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.example.newsapp.db.NewsRoom
import com.example.newsapp.db.dao.NewsDao


@Database(entities = [NewsRoom::class], version = 2)
abstract class NewsDataBase : RoomDatabase() {

    abstract fun newsDao(): NewsDao

    companion object {
        private var INSTANCE: NewsDataBase? = null

        fun getInstance(context: Context): NewsDataBase? {
            if (INSTANCE == null) {
                synchronized(NewsDataBase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        NewsDataBase::class.java, "newsDataBase"
                    ).fallbackToDestructiveMigration()
                        .build()
                }
            }
            return INSTANCE
        }

    }
}