package com.example.newsapp.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.example.newsapp.db.NewsRoom


@Dao
interface NewsDao {


    @Query("select  * From news_table ")
    fun getLiveNews(): LiveData<List<NewsRoom>>

    @Insert
    fun addNews(news: NewsRoom)
}