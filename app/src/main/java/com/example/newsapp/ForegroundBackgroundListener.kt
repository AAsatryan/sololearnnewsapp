package com.example.newsapp

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.support.v4.app.NotificationCompat
import com.example.newsapp.functions.NOTIFICATION_CHANNEL_ID
import com.example.newsapp.repository.NewsRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.*
import kotlin.concurrent.schedule

class ForegroundBackgroundListener(val context: Context) : LifecycleObserver {

    private var page = 0

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun startSomething() {
        val page = 0
        Timer().schedule(30000) {
            page + 10
            NewsRepository.getNewsAsync(page) {
            }

        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun stopSomething() {
        page = +10
        val knownLastId = NewsRepository.checkingList[0].id
        val newestId = NewsRepository.getLastNewsId(1)
        if (newestId != knownLastId) {
            notification()
        }

    }

    private fun notification() {
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant") val notificationChannel =
                NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_MAX)
            notificationChannel.description = "Guardian News"
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED
            notificationChannel.vibrationPattern = longArrayOf(500, 1000, 500, 1000)
            notificationChannel.enableVibration(true)
            notificationManager!!.createNotificationChannel(notificationChannel)
        }
        val notificationBuilder = NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
        notificationBuilder.setAutoCancel(true)
            .setDefaults(Notification.DEFAULT_ALL)
            .setWhen(System.currentTimeMillis())
            .setSmallIcon(R.drawable.guardian)
            //.setPriority(Notification.PRIORITY_MAX)
            .setContentTitle("Guardian Notification")
            .setContentText("Don't miss latest news")
            .setContentInfo("Information")
        notificationManager!!.notify(1, notificationBuilder.build())
    }
}
