package com.example.newsapp.repository

import android.arch.lifecycle.LiveData
import com.example.newsapp.activity.BaseApp
import com.example.newsapp.api.ApiService
import com.example.newsapp.db.NewsObject
import com.example.newsapp.db.NewsRoom
import com.example.newsapp.db.dao.NewsDao
import com.example.newsapp.functions.KEY
import kotlinx.coroutines.*
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.HttpException
import java.util.*
import kotlin.collections.ArrayList

object NewsRepository {

    private var newsDao: NewsDao? = null
    var liveNews: LiveData<List<NewsRoom>>? = null
    val checkingList = ArrayList<NewsObject>()

    init {
        val database = BaseApp().getDataBase()
        newsDao = database?.newsDao()
        liveNews = newsDao?.getLiveNews()
    }

    fun addNewsToLocalDB(newsRoom: NewsRoom) {
        GlobalScope.launch {
            newsDao?.addNews(newsRoom)
        }
    }

    fun getNewsAsync(page: Int, callback: (ArrayList<NewsObject>) -> Unit) = GlobalScope.launch(Dispatchers.Main) {
        try {
            val response = ApiService.createService().getNewsAsync(page, KEY).await()
            val list = createNewsObjectFromJson(response)

            callback(ArrayList(list.reversed()))
        } catch (e: HttpException) {
            e.response()
        }
    }

    fun getLastNewsId(page: Int): String {
        var id: String = ""
        runBlocking {
            try {
                val response = ApiService.createService().getNewsAsync(page, KEY).await()
                val responseString = response.string()
                val json = (JSONObject(responseString)["response"] as JSONObject)["results"] as JSONArray
                id = json.getJSONObject(0).optString("id")

            } catch (e: Exception) {
                e.message
            }
        }
        return id
    }

    private fun createNewsObjectFromJson(response: ResponseBody): ArrayList<NewsObject> {
        val newsList = ArrayList<NewsObject>()
        val responseString = response.string()
        val json = (JSONObject(responseString)["response"] as JSONObject)["results"] as JSONArray
        for (i in 0 until json.length()) {
            val id = json.getJSONObject(i).optString("id")
            val sectionName = json.getJSONObject(i).optString("sectionName")
            val trailText = json.getJSONObject(i).getJSONObject("fields").optString("trailText")
            val image = json.getJSONObject(i).getJSONObject("fields").optString("thumbnail")
            val article = json.getJSONObject(i).getJSONObject("fields").optString("bodyText")
            newsList.add(NewsObject(id, trailText, image, sectionName, article))
        }
        checkingList.addAll(newsList)
        return newsList
    }
}