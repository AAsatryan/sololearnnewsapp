package com.example.newsapp.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.newsapp.R
import com.example.newsapp.db.NewsObject
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.pin_news_item.view.*

class PinNewsAdapter(
    private val list: ArrayList<NewsObject>,
    val itemClick: (holder: ViewHolder, newsObject: NewsObject) -> Unit
) :
    RecyclerView.Adapter<PinNewsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.pin_news_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        if (!item.thumbnail.isNullOrEmpty()) {
            Picasso.get().load(item.thumbnail).into(holder.image)
        } else {
            holder.image.setImageResource(R.drawable.guardian)
        }
        holder.category.text = item.sectionName

        holder.itemView.setOnClickListener {
            itemClick(holder, item)
        }
    }

    fun addItem(pinItem: NewsObject) {
        list.add(pinItem)
        notifyDataSetChanged()
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image = itemView.pinedNewsImage!!
        val category = itemView.pinNewsCategory!!
    }
}