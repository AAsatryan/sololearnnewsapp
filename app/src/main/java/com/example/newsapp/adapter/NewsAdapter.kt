package com.example.newsapp.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.newsapp.R
import com.example.newsapp.db.NewsObject
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.news_item.view.*

class NewsAdapter(private val items: ArrayList<NewsObject>) :
    RecyclerView.Adapter<NewsAdapter.ViewHolder>() {

    var mListener: OnNewsItemClick? = null
    var isButtonClicked: Boolean = false
    var viewType: Int = 0


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return if (viewType == 1) {
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.grid_news_item, parent, false))
        } else {
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.news_item, parent, false))
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        if (!item.thumbnail.isNullOrEmpty()) {
            Picasso.get().load(item.thumbnail).placeholder(R.drawable.guardian).into(holder.image)
        } else {
            holder.image.setImageResource(R.drawable.guardian)
        }
        holder.text.text = item.trailText
        holder.title.text = item.sectionName
        holder.itemView.setOnClickListener {
            mListener?.onItemClicked(holder, item)
        }

        holder.pin.setOnClickListener {
            holder.pin.visibility = View.GONE
            mListener?.onItemPinClicked(item)
        }

        holder.save.setOnClickListener {
            holder.save.visibility = View.GONE
            mListener?.onSaveIconClicked(item)
        }

        if (viewType == 1) {
            holder.save.visibility = View.GONE
            holder.pin.visibility = View.GONE
        }
    }

    fun addToList(newNews: ArrayList<NewsObject>) {
        items.addAll(newNews)
        notifyDataSetChanged()
    }


    override fun getItemViewType(position: Int): Int {
        return if (isButtonClicked) 1 else 0
    }

    override fun getItemCount() = items.size


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.newsTitle!!
        val image = itemView.newsImage!!
        val text = itemView.newsHeader!!
        val pin = itemView.pin!!
        val save = itemView.save!!
    }

    interface OnNewsItemClick {

        fun onItemClicked(holder: NewsAdapter.ViewHolder, newsObject: NewsObject)

        fun onItemPinClicked(newsObject: NewsObject)

        fun onSaveIconClicked(newsObject: NewsObject)
    }
}