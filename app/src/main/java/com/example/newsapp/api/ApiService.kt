package com.example.newsapp.api

import com.example.newsapp.functions.BASE_URL
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit

interface ApiService {

    @GET("search?&show-fields=all")
    fun getNewsAsync(@Query("page") page: Int, @Query("api-key") key: String) : Deferred<ResponseBody>

    companion object {

        fun createService(): ApiService {
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .client(getOkHttpClient())
                .build()
                .create(ApiService::class.java)
        }

        private fun getOkHttpClient(): OkHttpClient {
            return OkHttpClient.Builder()
                .readTimeout(1000, TimeUnit.SECONDS)
                .connectTimeout(1000, TimeUnit.SECONDS)
                .build()
        }

    }
}