package com.example.newsapp.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.example.newsapp.viewModel.NewsViewModel
import com.example.newsapp.R
import com.example.newsapp.adapter.NewsAdapter
import com.example.newsapp.adapter.PinNewsAdapter
import com.example.newsapp.functions.EndlessRecyclerView
import com.example.newsapp.db.NewsObject
import com.example.newsapp.db.NewsRoom
import kotlinx.android.synthetic.main.activity_news.*
import kotlin.collections.ArrayList

class NewsActivity : AppCompatActivity(), NewsAdapter.OnNewsItemClick {


    private lateinit var viewModel: NewsViewModel
    private val pinNewsList = ArrayList<NewsObject>()
    private var nAdapter: NewsAdapter? = null
    private var pAdapter: PinNewsAdapter? = null
    private var isClicked = false
    private var page = 10


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news)
        viewModel = ViewModelProviders.of(this).get(NewsViewModel::class.java)
        openScreenByConnection()
        drawHorizontalRecyclerView()
        changeListType.setOnClickListener {
            changeRecyclerViewStyle()
        }

    }

    private fun changeRecyclerViewStyle() {
        isClicked = !isClicked
        nAdapter!!.isButtonClicked = isClicked
        if (isClicked) {
            newsRecyclerView.layoutManager = GridLayoutManager(this, 2)
        } else {
            newsRecyclerView.layoutManager = LinearLayoutManager(this)

        }
    }

    private fun openScreenByConnection() {
        loading.visibility = View.VISIBLE
        if (viewModel.isConnectedToNetwork(this)) {
            viewModel.loadNewsFromNetwork(page) {
                loading.visibility = View.GONE
                drawVerticalRecyclerView(it)
            }
        } else {
            viewModel.getLiveNews?.observe(this, Observer<List<NewsRoom>> {
                val newsObject = ArrayList<NewsObject>()
                for (i in it!!) {
                    newsObject.add(NewsObject(i.id, i.trailText, i.thumbnail, i.sectionName, i.article))
                }
                loading.visibility = View.GONE
                drawVerticalRecyclerView(newsObject)
                nAdapter?.viewType = 1
            })
        }

    }

    private fun drawHorizontalRecyclerView() {
        pAdapter = PinNewsAdapter(pinNewsList) { holder, item ->
            val intent = Intent(this, ArticleActivity::class.java)
            intent.putExtra("news_image", item.thumbnail)
            intent.putExtra("news_title", item.sectionName)
            intent.putExtra("news_info", item.trailText)
            intent.putExtra("news_body", item.article)

            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, holder.image, "transitionImage")

            startActivity(intent, options.toBundle())

        }
        pinNewsRecyclerView.adapter = pAdapter
        pinNewsRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
    }

    private fun drawVerticalRecyclerView(list: ArrayList<NewsObject>) {
        newsRecyclerView.layoutManager = LinearLayoutManager(this)
        nAdapter = NewsAdapter(list)
        nAdapter!!.mListener = this
        newsRecyclerView.adapter = nAdapter
        newsRecyclerView.addOnScrollListener(object : EndlessRecyclerView(this, newsRecyclerView.layoutManager!!) {
            override fun onLoadMore() {
                if (viewModel.isConnectedToNetwork(this@NewsActivity)) {
                    page += 10
                    viewModel.loadNewsFromNetwork(page) {
                        nAdapter?.addToList(it)
                    }
                }
            }

        })
    }

    override fun onItemClicked(holder: NewsAdapter.ViewHolder, newsObject: NewsObject) {
        val intent = Intent(this, ArticleActivity::class.java)
        intent.putExtra("news_image", newsObject.thumbnail)
        intent.putExtra("news_title", newsObject.sectionName)
        intent.putExtra("news_info", newsObject.trailText)
        intent.putExtra("news_body", newsObject.article)

        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, holder.image, "transitionImage")

        startActivity(intent, options.toBundle())
    }

    override fun onItemPinClicked(newsObject: NewsObject) {
        pAdapter?.addItem(newsObject)
        nAdapter?.mListener = this

    }

    override fun onSaveIconClicked(newsObject: NewsObject) {
        viewModel.addNewsToLocalDB(newsObject)
    }

}
