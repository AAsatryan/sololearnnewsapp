package com.example.newsapp.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.transition.TransitionInflater
import android.view.MenuItem
import android.view.WindowManager
import com.example.newsapp.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.article_activity.*

class ArticleActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.article_activity)
        val articleTitle = intent.getStringExtra("news_title")
        val articleInfo = intent.getStringExtra("news_info")
        val articleImage = intent.getStringExtra("news_image")
        val articleBody = intent.getStringExtra("news_body")
        window.setFlags(
            WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
            WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
        )
        window.sharedElementEnterTransition =
            TransitionInflater.from(this).inflateTransition(R.transition.shared_element_transition)
        setupToolbar(articleTitle)
        if (!articleImage.isNullOrEmpty()) {
            Picasso.get().load(articleImage).into(imageNews)
        } else {
            imageNews.setImageResource(R.drawable.guardian)
        }
        textTitle.text = articleInfo
        textDescription.text = articleBody

    }

    private fun setupToolbar(title: String) {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
        supportActionBar?.title = title
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean =
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> false
        }
}