package com.example.newsapp.activity

import android.app.AlarmManager
import android.app.Application
import android.app.PendingIntent
import android.arch.lifecycle.ProcessLifecycleOwner
import android.content.Context
import android.content.Intent
import com.example.newsapp.ForegroundBackgroundListener
import com.example.newsapp.db.database.NewsDataBase
import java.util.*

class BaseApp : Application() {

    override fun onCreate() {
        super.onCreate()
        getDataBase()
        ProcessLifecycleOwner.get()
            .lifecycle
            .addObserver(
                ForegroundBackgroundListener(this)
            )
    }



    fun getDataBase() = NewsDataBase.getInstance(this)
}