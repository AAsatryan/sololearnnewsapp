package com.example.newsapp.viewModel

import android.app.*
import android.arch.lifecycle.AndroidViewModel
import android.content.Context
import com.example.newsapp.repository.NewsRepository
import com.example.newsapp.db.NewsObject
import com.example.newsapp.db.NewsRoom
import android.net.ConnectivityManager
import java.util.*
import kotlin.collections.ArrayList


class NewsViewModel(application: Application) : AndroidViewModel(application) {

    private val newsRepository: NewsRepository = NewsRepository
    val getLiveNews = newsRepository.liveNews

    fun loadNewsFromNetwork(page: Int, callback: (ArrayList<NewsObject>) -> Unit) {
        newsRepository.getNewsAsync(page) {
            callback(it)
        }

    }

    fun addNewsToLocalDB(newsObject: NewsObject) {
        val news = NewsRoom(
            newsObject.id!!,
            newsObject.trailText,
            newsObject.thumbnail,
            newsObject.sectionName,
            newsObject.article
        )
        newsRepository.addNewsToLocalDB(news)
    }

    fun isConnectedToNetwork(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = connectivityManager.activeNetworkInfo

        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }


}